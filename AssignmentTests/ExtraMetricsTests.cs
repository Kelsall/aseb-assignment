﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEBassignment;

namespace AssignmentTests
{
    [TestClass]
    public class ExtraMetricsTests
    {
        AdditionalMetrics extraMetrics = new AdditionalMetrics();

        [TestMethod]
        public void findNormalisedPower_findsCorrectAverage()
        {
            //Use the Training Peaks software values to test that the value given by this software for the average normalised power is correct
            //Training Peaks Value: 228 W

            //Arrange
            double returnedValue, expectedValue;
            string fileName = "F:\\Software B\\Data Files\\ASDBExampleCycleComputerData.hrm";
            expectedValue = 232;

            //Act
            returnedValue = extraMetrics.findNormalisedPower(fileName);
            returnedValue = Math.Round(returnedValue);

            //Assert
            Assert.AreEqual(expectedValue, returnedValue, "The incorrect normalised power was found!");

        }

        [TestMethod]
        public void findIntensityFactor_findsCorrectValue()
        {
            //Use the Training Peaks software values to test that the value given by this software for the intensity factor is correct
            //Training Peaks Value: 0.72
            //Training Peaks NP: 232

            //Arrange
            double returnedValue, expectedValue, NP, FTP;
            expectedValue = 0.72;
            NP = 232;
            FTP = 320;

            //Act
            returnedValue = extraMetrics.findIntensityFactor(NP, FTP);
            returnedValue = Math.Round(returnedValue, 2);

            //Assert
            Assert.AreEqual(expectedValue, returnedValue, "The incorrect intensity factor was found!");

        }

        [TestMethod]
        public void findTrainingTestScore_findsCorrectValue()
        {
            //Use the Training Peaks software values to test that the value given by this software for the training test score is correct
            //Training Peaks value: 57.7
            //Training Peaks NP: 232
            //Training Peaks IF: 0.72

            //Arrange
            string fileName = "F:\\Software B\\Data Files\\ASDBExampleCycleComputerData.hrm";
            double returnedValue, expectedValue, NP, intensityFactor, FTP;
            expectedValue = 57.7;
            NP = 232;
            intensityFactor = 0.72;
            FTP = 320;

            //Act
            returnedValue = extraMetrics.findTrainingStressScore(fileName, NP, intensityFactor, FTP);
            returnedValue = Math.Round(returnedValue, 1);

            //Assert
            Assert.AreEqual(expectedValue, returnedValue, "The incorrect training test score was found!");

        }
    }
}
