﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEBassignment;

namespace AssignmentTests
{
    [TestClass]
    public class ReadDataTests
    {
        [TestMethod]
        public void findLengthInSeconds_findsLength()
        {
            //arrange
            double returnedValue, expectedValue = 3661;
            string testLength = "01:01:01";
            FindLengthOfRide findLength = new FindLengthOfRide();

            //act
            returnedValue = findLength.findLengthInSeconds(testLength);

            //assert
            Assert.AreEqual(expectedValue, returnedValue, "Time not calculated correctly!");
        }

        [TestMethod]
        public void sMode_readUnit_knownEuro()
        {
            //arrange
            string sModeTest = "111111100";                 //Eight character is 0, which means the data has been recorded in Euro units
            bool returnedValue, expectedValue = true;
            ProcessSMode findUnit = new ProcessSMode();

            //act
            returnedValue = findUnit.readUnit(sModeTest);

            //assert
            Assert.AreEqual(expectedValue, returnedValue, "The incorrect unit was found!");
        }

        [TestMethod]
        public void dateToUK_correctFormat()
        {
            //arrange
            ProcessDate formatDate = new ProcessDate();
            string newDate = "20141013", returnedValue, expectedValue = "13/10/2014";

            //act
            returnedValue = formatDate.dateToUK(newDate);

            //assert
            Assert.AreEqual(expectedValue, returnedValue, "The date wasn't formatted correctly!");
        }
    }
}
