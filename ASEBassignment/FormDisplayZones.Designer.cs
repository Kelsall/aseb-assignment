﻿namespace ASEBassignment
{
    partial class FormDisplayZones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControlZones = new ZedGraph.ZedGraphControl();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zedGraphControlZones
            // 
            this.zedGraphControlZones.Location = new System.Drawing.Point(12, 12);
            this.zedGraphControlZones.Name = "zedGraphControlZones";
            this.zedGraphControlZones.ScrollGrace = 0D;
            this.zedGraphControlZones.ScrollMaxX = 0D;
            this.zedGraphControlZones.ScrollMaxY = 0D;
            this.zedGraphControlZones.ScrollMaxY2 = 0D;
            this.zedGraphControlZones.ScrollMinX = 0D;
            this.zedGraphControlZones.ScrollMinY = 0D;
            this.zedGraphControlZones.ScrollMinY2 = 0D;
            this.zedGraphControlZones.Size = new System.Drawing.Size(860, 507);
            this.zedGraphControlZones.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(13, 526);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(859, 24);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FormDisplayZones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.ControlBox = false;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.zedGraphControlZones);
            this.Name = "FormDisplayZones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Power & Heart Rate Zones";
            this.Load += new System.EventHandler(this.FormDisplayZones_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControlZones;
        private System.Windows.Forms.Button btnClose;
    }
}