﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ASEBassignment
{
    public partial class FormDisplayGraphs : Form
    {
        Form1 dataForm;
        FormDisplayZones zoneForm;
        Boolean distancePlot, speedPlot, heartPlot, powerPlot, altitudePlot, selectableDataComparison;
        LineItem curve, curveD, curveS, curveHR, curveP, curveA;
        int newMinX, newMaxX, NoOfRows;
        string fName;

        public FormDisplayGraphs()
        {
            InitializeComponent();
            distancePlot = speedPlot = heartPlot = powerPlot = altitudePlot = true;
            selectableDataComparison = false;
        }

        private void showDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataForm.Show();
        }

        private void FormDisplayGraphs_Load(object sender, EventArgs e)
        {
            double min, max;

            selectFile();
            this.Size = new Size(1080, 735);
            dataForm = new Form1(fName);
            createGraph(zedGraphControl1);
            //Get min and max values of the X Axis
            min = Convert.ToDouble(zedGraphControl1.GraphPane.XAxis.Scale.Min.ToString());
            max = Convert.ToDouble(zedGraphControl1.GraphPane.XAxis.Scale.Max.ToString());
            min = Math.Floor(min);
            max = Math.Ceiling(max);
            newMinX = Convert.ToInt32(min);
            newMaxX = Convert.ToInt32(max);
            txtMinX.Text = newMinX.ToString();
            txtMaxX.Text = newMaxX.ToString();
            NoOfRows = Convert.ToInt16(dataForm.txtNoRows.Text);
            calculateData("start");
            lblFilename.Text = fName;
        }

        public void selectFile()
        {
            OpenFileDialog chooseFile = new OpenFileDialog();
            chooseFile.Filter = "All files (*.*)|*.*";
            chooseFile.InitialDirectory = @"F:\Software B\Data Files";
            chooseFile.Title = "Select a file";
            if (chooseFile.ShowDialog(this) == DialogResult.OK)
            {
                fName = chooseFile.FileName;
            }
        }

        private void graphZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            double max, min;
            string mode;

            //Get min and max values of the new X Axis
            min = Convert.ToDouble(sender.GraphPane.XAxis.Scale.Min.ToString());
            max = Convert.ToDouble(sender.GraphPane.XAxis.Scale.Max.ToString());
            min = Math.Floor(min);
            max = Math.Ceiling(max);
            newMinX = Convert.ToInt32(min);
            newMaxX = Convert.ToInt32(max);
            if (selectableDataComparison)
            {
                txtminX2.Text = newMinX.ToString();
                txtmaxX2.Text = newMaxX.ToString();
                mode = "select2";
            }
            else
            {
                txtminX1.Text = newMinX.ToString();
                txtmaxX1.Text = newMaxX.ToString();
                mode = "select1";
            }
            this.Size = new Size(1080, 925);

            calculateData(mode);
        }

        private void calculateData(string mode)
        {
            double distance, average, max, min;

            if (distancePlot)
            {
                distance = distanceTravelled();

                if (mode == "start")
                    txtDistance.Text = distance.ToString();
                if (mode == "select1")
                    txtD1.Text = distance.ToString();
                if (mode == "select2") 
                    txtD2.Text = distance.ToString();
            }
            else
            {
                txtDistance.Text = "N/A";
            }

            if (speedPlot)
            {
                average = findAverage(3);
                max = findMax(3);
                if (mode == "start")
                {
                    txtAvSpeed.Text = average.ToString();
                    txtMaxSpeed.Text = max.ToString();
                }
                if (mode == "select1")
                {
                    txtMS1.Text = max.ToString();
                    txtAS1.Text = average.ToString();
                }
                if (mode == "select2")
                {
                    txtMS2.Text = max.ToString();
                    txtAS2.Text = average.ToString();
                }
            }
            else
            {
                txtAvSpeed.Text = "N/A";
                txtMaxSpeed.Text = "N/A";
            }

            if (heartPlot)
            {
                average = findAverage(2);
                max = findMax(2);
                min = minHR();

                if (mode == "start")
                {
                    txtAvHR.Text = average.ToString();
                    txtMaxHR.Text = max.ToString();
                    txtMinHR.Text = min.ToString();
                }
                if (mode == "select1")
                {
                    txtminHR1.Text = min.ToString();
                    txtAHR1.Text = average.ToString();
                    txtmaxHR1.Text = max.ToString();
                }
                if (mode == "select2")
                {
                    txtminHR2.Text = min.ToString();
                    txtAHR2.Text = average.ToString();
                    txtmaxHR2.Text = max.ToString();
                }

            }
            else
            {
                txtAvHR.Text = "N/A";
                txtMaxHR.Text = "N/A";
                txtMinHR.Text = "N/A";
            }

            if (powerPlot)
            {
                average = findAverage(6);
                max = findMax(6);

                if (mode == "start")
                {
                    txtAvPower.Text = average.ToString();
                    txtMaxPower.Text = max.ToString();
                }
                if (mode == "select1")
                {
                    txtMP1.Text = max.ToString();
                    txtAP1.Text = average.ToString();
                }
                if (mode == "select2")
                {
                    txtMP2.Text = max.ToString();
                    txtAP2.Text = average.ToString();
                }
            }
            else
            {
                txtAvPower.Text = "N/A";
                txtMaxPower.Text = "N/A";
            }

            if (altitudePlot)
            {
                average = findAverage(5);
                max = findMax(5);

                if (mode == "start")
                {
                    txtAvAlt.Text = average.ToString();
                    txtMaxAlt.Text = max.ToString();
                }
                if (mode == "select1")
                {
                    txtMA1.Text = max.ToString();
                    txtAA1.Text = average.ToString();
                }
                if (mode == "select2")
                {
                    txtMA2.Text = max.ToString();
                    txtAA2.Text = average.ToString();
                }
            }
            else
            {
                txtAvAlt.Text = "N/A";
                txtMaxAlt.Text = "N/A";
            }
        }

        public void calculateExtraMetrics()
        {
            AdditionalMetrics extraMetrics = new AdditionalMetrics();
            double FTP, NP, intensityFactor, TSS;

            //Get FTP
            FTP = Convert.ToDouble(txtFTP.Text);

            //Find Normailised Power
            NP = extraMetrics.findNormalisedPower(fName);

            //Find Intensity factor
            intensityFactor = extraMetrics.findIntensityFactor(NP, FTP);

            //Find Training Stress Score
            TSS = extraMetrics.findTrainingStressScore(fName, NP, intensityFactor, FTP);

            //Display Metrics
            NP = Math.Round(NP);
            txtNP.Text = NP.ToString();
            txtIF.Text = intensityFactor.ToString("0.##");
            TSS = Math.Round(TSS);
            txtTSS.Text = TSS.ToString();

            //Change colour of textbox to draw the eye
            txtNP.BackColor = System.Drawing.Color.LightYellow;
            txtIF.BackColor = System.Drawing.Color.LightYellow;
            txtTSS.BackColor = System.Drawing.Color.LightYellow;
        }

        public double distanceTravelled()
        {
            double distanceSum = 0;
            int columnIndex = 3, i;

            if (newMaxX > NoOfRows)     //Validation for when maxX is out of the bounds of the dataGridView
            {
                newMaxX = NoOfRows;
            }

            for (i = newMinX; i < newMaxX; i++)
            {
                distanceSum += Convert.ToDouble(dataForm.dgvData.Rows[i].Cells[columnIndex].Value);
            }

            distanceSum = distanceSum / 3600;
            distanceSum = Math.Round(distanceSum, 1);

            return distanceSum;
        }

        public double findAverage(int columnIndex)
        {
            double average, sum = 0;
            int i, count = 0;

            if (newMaxX > NoOfRows)     //Validation for when maxX is out of the bounds of the dataGridView
            {
                newMaxX = NoOfRows;
            }

            for (i = newMinX; i < newMaxX; i++)
            {
                sum += Convert.ToDouble(dataForm.dgvData.Rows[i].Cells[columnIndex].Value);
                count++;
            }

            average = sum / count;
            average = Math.Round(average, 0);

            return average;
        }

        public double findMax(int columnIndex)
        {
            double maximum = 0, newMax;
            int i;

            if (newMaxX > NoOfRows)     //Validation for when maxX is out of the bounds of the dataGridView
            {
                newMaxX = NoOfRows;
            }

            for (i = newMinX; i < newMaxX; i++)
            {
                newMax = Convert.ToDouble(dataForm.dgvData.Rows[i].Cells[columnIndex].Value);
                if (Convert.ToInt16(newMax) > maximum)
                {
                    maximum = newMax;
                }
            }

            return maximum;
        }

        public int minHR()
        {
            int columnIndex = 2, i, minHR;
            double heartRate, miniHR = findMax(columnIndex);

            minHR = Convert.ToInt16(miniHR);
            if (newMaxX > NoOfRows)     //Validation for when maxX is out of the bounds of the dataGridView
            {
                newMaxX = NoOfRows;
            }

            for (i = newMinX; i < newMaxX; i++)
            {
                heartRate = Convert.ToDouble(dataForm.dgvData.Rows[i].Cells[columnIndex].Value);
                if (Convert.ToInt16(heartRate) < minHR && Convert.ToInt16(heartRate) != 0)
                {
                    minHR = Convert.ToInt16(heartRate);
                }
            }

            return minHR;
        }

        // Build the Chart
        private void createGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            // Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(All Data)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = " ";

            
            /*// Change XAxis so it accepts time type
            myPane.XAxis.Type = AxisType.Date;
            myPane.XAxis.Scale.Format = "HH:mm:ss";
            myPane.XAxis.Scale.MajorUnit = DateUnit.Second;
            myPane.XAxis.Scale.MinorUnit = DateUnit.Millisecond;
            myPane.XAxis.Scale.Min = DateTime.Now.Subtract(new TimeSpan(0, 0, 10, 0, 0)).ToOADate();
            myPane.XAxis.Scale.Max = DateTime.Now.ToOADate();
            */

            // Make up some data arrays based data from file
            double x = 0, y1, y2, y3, y4, y5, distanceSum = 0;;
            int columnIndex;
            PointPairList listD = new PointPairList();
            PointPairList listS = new PointPairList();
            PointPairList listHR = new PointPairList();
            PointPairList listP = new PointPairList();
            PointPairList listA = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 3;
                distanceSum += Convert.ToDouble(dgvRow.Cells[columnIndex].Value);
                y1 = distanceSum / 3600;
                y2 = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));
                columnIndex = 2;
                y3 = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));
                columnIndex = 6;
                y4 = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));
                columnIndex = 5;
                y5 = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));

                listD.Add(x, y1);
                listS.Add(x, y2);
                listHR.Add(x, y3);
                listP.Add(x, y4);
                listA.Add(x, y5);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curveD = myPane.AddCurve("Distance", listD, Color.Green, SymbolType.None);
            curveS = myPane.AddCurve("Speed", listS, Color.Black, SymbolType.None);
            curveHR = myPane.AddCurve("Heart Rate", listHR, Color.Red, SymbolType.None);
            curveP = myPane.AddCurve("Power", listP, Color.Blue, SymbolType.None);
            curveA = myPane.AddCurve("Altitude", listA, Color.Purple, SymbolType.None);

            //Set x-axis Scale
            myPane.XAxis.Scale.Max = x + 20;

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //Set plot variables
            distancePlot  = true;
            speedPlot = true;
            heartPlot = true;
            powerPlot = true;
            altitudePlot = true;

            //Disable selectable zooming
            //zgc.ZoomButtons = MouseButtons.None;
            //zgc.ZoomButtons2 = MouseButtons.None;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;

            // Make both curves thicker
            //myCurve.Line.Width = 2.0F;

            // Fill the area under the curves
            //myCurve.Line.Fill = new Fill(Color.White, Color.Red, 45F);

            // Increase the symbol sizes, and fill them with solid white
            //myCurve.Symbol.Size = 8.0F;
            //myCurve.Symbol.Fill = new Fill(Color.White);

            

            // Add a caption and an arrow
            //TextObj myText = new TextObj("Interesting\nPoint", 230F, 70F);
            //myText.FontSpec.FontColor = Color.Red;
            //myText.Location.AlignH = AlignH.Center;
            //myText.Location.AlignV = AlignV.Top;
            //myPane.GraphObjList.Add(myText);
            //ArrowObj myArrow = new ArrowObj(Color.Red, 12F, 230F, 70F, 280F, 55F);
            //myPane.GraphObjList.Add(myArrow);
        }

        public void distanceGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(Distance Travelled)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = "Distance (KM)";

            double x = 0, y, distanceSum = 0;
            int columnIndex;
            PointPairList list = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 3;
                distanceSum += Convert.ToDouble(dgvRow.Cells[columnIndex].Value);
                y = distanceSum / 3600;

                list.Add(x, y);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curve = myPane.AddCurve("Distance", list, Color.Green, SymbolType.None);

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //set plot variable
            distancePlot = true;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;
        }

        public void speedGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(Speed)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = "Speed (KM/h)";

            double x = 0, y;
            int columnIndex;
            PointPairList list = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 3;
                y = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));

                list.Add(x, y);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curve = myPane.AddCurve("Speed", list, Color.Black, SymbolType.None);

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //set plot variable
            speedPlot = true;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;
        }

        public void heartGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(Heart Rate)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = "Heart Rate (BPM)";

            double x = 0, y;
            int columnIndex;
            PointPairList list = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 2;
                y = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));

                list.Add(x, y);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curve = myPane.AddCurve("Heart Rate", list, Color.Red, SymbolType.None);

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //set plot variable
            heartPlot = true;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;
        }

        public void powerGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(Power)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = "Power (W)";

            double x = 0, y;
            int columnIndex;
            PointPairList list = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 6;
                y = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));

                list.Add(x, y);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curve = myPane.AddCurve("Power", list, Color.Blue, SymbolType.None);

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //set plot variable
            powerPlot = true;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;
        }

        public void altitudeGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //Clear previous graph data
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Your Ride Summary\n(Altitude)";
            myPane.XAxis.Title.Text = "Interval";
            myPane.YAxis.Title.Text = "Altitude (KM)";

            double x = 0, y;
            int columnIndex;
            PointPairList list = new PointPairList();
            foreach (DataGridViewRow dgvRow in dataForm.dgvData.Rows)
            {
                columnIndex = 5;
                y = (Convert.ToDouble(dgvRow.Cells[columnIndex].Value));

                list.Add(x, y);
                x++; //should add the increment value to x  OR  make x equal to time
            }

            // Generate curves
            curve = myPane.AddCurve("Altitude", list, Color.Purple, SymbolType.None);

            // Tell ZedGraph to refigure the axes since the data have changed
            zgc.AxisChange();

            // Change the color of the title
            //myPane.Title.FontSpec.FontColor = Color.Green;

            // Add gridlines to the plot, and make them gray
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.Color = Color.LightGray;
            myPane.YAxis.MajorGrid.Color = Color.LightGray;

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;

            // Add a background gradient fill to the axis frame
            myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 210), -45F);

            //Refresh graph
            zgc.Refresh();

            //set plot variable
            altitudePlot = true;

            //Get values from zoom event
            zgc.ZoomEvent += graphZoomEvent;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void distanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            distanceGraph(zedGraphControl1);
        }

        private void showAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createGraph(zedGraphControl1);
        }

        private void speedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            speedGraph(zedGraphControl1);
        }

        private void heartRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            heartGraph(zedGraphControl1);
        }

        private void powerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            powerGraph(zedGraphControl1);
        }

        private void altitudeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            altitudeGraph(zedGraphControl1);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (speedPlot)
            {
                //Remove graph plot
                zedGraphControl1.GraphPane.CurveList.Remove(curveS);
                zedGraphControl1.Refresh();
                speedPlot = false;
                calculateData("start");
            }
            else
            {
                //plot graph
                zedGraphControl1.GraphPane.CurveList.Add(curveS);
                zedGraphControl1.Refresh();
                speedPlot = true;
                calculateData("start");
            }
        }

        private void cBoxDistance_CheckedChanged(object sender, EventArgs e)
        {
            if (distancePlot)
            {
                //Remove graph plot
                zedGraphControl1.GraphPane.CurveList.Remove(curveD);
                zedGraphControl1.Refresh();
                distancePlot = false;
                calculateData("start");
            }
            else
            {
                //plot graph
                zedGraphControl1.GraphPane.CurveList.Add(curveD);
                zedGraphControl1.Refresh();
                distancePlot = true;
                calculateData("start");
            }
        }

        private void cBoxHeart_CheckedChanged(object sender, EventArgs e)
        {
            if (heartPlot)
            {
                //Remove graph plot
                zedGraphControl1.GraphPane.CurveList.Remove(curveHR);
                zedGraphControl1.Refresh();
                heartPlot = false;
                calculateData("start");
            }
            else
            {
                //plot graph
                zedGraphControl1.GraphPane.CurveList.Add(curveHR);
                zedGraphControl1.Refresh();
                heartPlot = true;
                calculateData("start");
            }
        }

        private void cBoxPower_CheckedChanged(object sender, EventArgs e)
        {
            if (powerPlot)
            {
                //Remove graph plot
                zedGraphControl1.GraphPane.CurveList.Remove(curveP);
                zedGraphControl1.Refresh();
                powerPlot = false;
                calculateData("start");
            }
            else
            {
                //plot graph
                zedGraphControl1.GraphPane.CurveList.Add(curveP);
                zedGraphControl1.Refresh();
                powerPlot = true;
                calculateData("start");
            }
        }

        private void cBoxAltitude_CheckedChanged(object sender, EventArgs e)
        {
            if (altitudePlot)
            {
                //Remove graph plot
                zedGraphControl1.GraphPane.CurveList.Remove(curveA);
                zedGraphControl1.Refresh();
                altitudePlot = false;
                calculateData("start");
            }
            else
            {
                //plot graph
                zedGraphControl1.GraphPane.CurveList.Add(curveA);
                zedGraphControl1.Refresh();
                altitudePlot = true;
                calculateData("start");
            }
        }

        private void btnPowerZones_Click(object sender, EventArgs e)
        {
            string zoneType = "FTP";
            double limit = 0;
            int min = Convert.ToInt16(txtMinX.Text), max = Convert.ToInt16(txtMaxX.Text);

            if (string.IsNullOrWhiteSpace(txtFTP.Text))     //Validation to ensure user has entered data
            {
                MessageBox.Show("Please enter your FTP.");
            }
            else
            {
                limit = Convert.ToDouble(txtFTP.Text);
                zoneForm = new FormDisplayZones(zoneType, limit, min, max, fName);
                zoneForm.Show();
            }
        }

        private void btnHRZones_Click(object sender, EventArgs e)
        {
            string zoneType = "HR";
            double limit = 0;
            int min = Convert.ToInt16(txtMinX.Text), max = Convert.ToInt16(txtMaxX.Text);

            if (string.IsNullOrWhiteSpace(txtUserHR.Text))     //Validation to ensure user has entered data
            {
                MessageBox.Show("Please enter your Maximum Heart Rate.");
            }
            else
            {
                limit = Convert.ToDouble(txtUserHR.Text);
                zoneForm = new FormDisplayZones(zoneType, limit, min, max, fName);
                zoneForm.Show();
            }
        }

        private void btnMetrics_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtFTP.Text))     //Validation to ensure user has entered data
            {
                MessageBox.Show("Please enter your FTP.");
            }
            else
            {
                calculateExtraMetrics();
            }
        }

        private void txtMinX_KeyDown(object sender, KeyEventArgs e)     //NEED TO COMPLETE
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Set value to minimum x value
                zedGraphControl1.GraphPane.XAxis.Scale.Min = Convert.ToInt16(txtMinX.Text);
            }
        }

        private void txtMaxX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Set value to maximum x value
                zedGraphControl1.GraphPane.XAxis.Scale.Max = Convert.ToInt16(txtMaxX.Text);
            }
        }

        private void clearSelectedData1()
        {
            txtMS1.Text    = "";
            txtAS1.Text    = "";
            txtD1.Text     = "";
            txtminHR1.Text = "";
            txtAHR1.Text   = "";
            txtmaxHR1.Text = "";
            txtAP1.Text    = "";
            txtMP1.Text    = "";
            txtAA1.Text    = "";
            txtMA1.Text    = "";
            txtminX1.Text  = "";
            txtmaxX1.Text  = "";
        }

        private void clearSelectedData2()
        {
            txtMS2.Text = "";
            txtAS2.Text = "";
            txtD2.Text = "";
            txtminHR2.Text = "";
            txtAHR2.Text = "";
            txtmaxHR2.Text = "";
            txtAP2.Text = "";
            txtMP2.Text = "";
            txtAA2.Text = "";
            txtMA2.Text = "";
            txtminX2.Text = "";
            txtmaxX2.Text = "";
        }

        private void btnClear1_Click(object sender, EventArgs e)
        {
            clearSelectedData1();
        }

        private void btnClear2_Click(object sender, EventArgs e)
        {
            clearSelectedData2();
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            clearSelectedData1();
            clearSelectedData2();
            selectableDataComparison = false;
            zedGraphControl1.ZoomOutAll(zedGraphControl1.GraphPane);
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            selectableDataComparison = true;
            zedGraphControl1.ZoomOutAll(zedGraphControl1.GraphPane);
        }
    }

    public class AdditionalMetrics
    {
        public double findNormalisedPower(string fName)
        {
            int length, Interval, count, i, j, k, l, pointer, NoOfValues;
            double sum, normalisedPower, NoOfRows, sumOfArray, avOfArray;
            double[] averageArray;
            Form1 dataForm = new Form1(fName);

            //Initilise variables
            k = 0;
            length = dataForm.dgvData.Rows.Count;
            Interval = Convert.ToInt16(dataForm.txtInterval.Text);
            NoOfRows = length / Interval;
            sum = 0;
            sumOfArray = 0;
            pointer = 30;                           
            NoOfValues = (length - 30) / 30;        
            averageArray = new double[NoOfValues];

            /////////////
            //  FORMULA//
            //  1) starting at the 30s mark, calculate a rolling 30s average (of the preceeding time points, obviously). 
            //  2) raise all the values obtained in step #1 to the 4th power. 
            //  3) take the average of all of the values obtained in step #2. 
            //  4) take the 4th root of the value obtained in step #3.
            /////////////

            //Get rolling average
            for (i = 0; i < NoOfValues; i++)
            {
                for (j = 1; j < 31; j++)
                {
                    //Get power value from datagrid at pointer
                    sum += Convert.ToDouble(dataForm.dgvData.Rows[pointer].Cells[6].Value);
                    pointer++;
                }
                averageArray[k] = sum / 30;
                k++;
                sum = 0;
            }

            //Raise values by 4th power
            for (l = 0; l < averageArray.Length; l++)
                //Math.Pow doesn't seem to work correctly
                averageArray[l] = averageArray[l] * averageArray[l] * averageArray[l] * averageArray[l];

            //Get average of new values
            for (count = 0; count < averageArray.Length; count++)
                sumOfArray += averageArray[count];
            avOfArray = sumOfArray / averageArray.Length;

            //Get 4th root of the new average
            avOfArray = Math.Sqrt(avOfArray);
            avOfArray = Math.Sqrt(avOfArray);

            //Get Normalised Power
            normalisedPower = avOfArray;
            return normalisedPower;


            //if (Interval == 1)
            //{
            /*}
            if (Interval == 4)
            {
                pointer = 7;       //Miss first 30[28] seconds
                NoOfValues = length - 30 / 7;

                for (i = 0; i < NoOfValues; i++)
                {
                    for (j = 1; j < 8; j++)
                    {
                        //Get power value from datagrid at pointer
                        sum += Convert.ToDouble(dataForm.dgvData.Rows[pointer].Cells[6].Value);
                        pointer++;
                    }
                    averageArray[k] = sum / 7;
                    k++;
                }
            }*/
        }

        public double findIntensityFactor(double NP, double FTP)
        {
            FormDisplayGraphs graphForm = new FormDisplayGraphs();
            double intensityFactor;

            intensityFactor = NP / FTP;

            return intensityFactor;
        }

        public double findTrainingStressScore(string fName, double NP, double intensityFactor, double FTP)
        {
            Form1 dataForm = new Form1(fName);
            double TSS;
            int length;

            length = dataForm.dgvData.Rows.Count;

            TSS = ((length * NP * intensityFactor) / (FTP * 3600)) * 100;

            return TSS;
        }
    }
}
