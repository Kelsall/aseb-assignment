﻿namespace ASEBassignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lboText = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtStartTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtSMode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rBtnEuro = new System.Windows.Forms.RadioButton();
            this.rBtnUS = new System.Windows.Forms.RadioButton();
            this.lblUnits = new System.Windows.Forms.Label();
            this.txtMaxSpeed = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDistance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaxAlt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaxPower = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMinHR = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAvSpeed = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAvHR = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAvAlt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAvPower = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMaxHR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.txtNoRows = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lboText
            // 
            this.lboText.FormattingEnabled = true;
            this.lboText.Location = new System.Drawing.Point(12, 308);
            this.lboText.Name = "lboText";
            this.lboText.Size = new System.Drawing.Size(10, 4);
            this.lboText.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "File:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(268, 53);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 13);
            this.lblDate.TabIndex = 7;
            this.lblDate.Text = "Date:";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(307, 50);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(72, 20);
            this.txtDate.TabIndex = 8;
            // 
            // txtStartTime
            // 
            this.txtStartTime.Location = new System.Drawing.Point(449, 50);
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.Size = new System.Drawing.Size(72, 20);
            this.txtStartTime.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Start Time:";
            // 
            // txtLength
            // 
            this.txtLength.Location = new System.Drawing.Point(576, 50);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(72, 20);
            this.txtLength.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(527, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Length:";
            // 
            // txtInterval
            // 
            this.txtInterval.Location = new System.Drawing.Point(705, 50);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(72, 20);
            this.txtInterval.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(654, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Interval:";
            // 
            // dgvData
            // 
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(15, 76);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersWidth = 43;
            this.dgvData.Size = new System.Drawing.Size(863, 400);
            this.dgvData.TabIndex = 15;
            // 
            // txtSMode
            // 
            this.txtSMode.Location = new System.Drawing.Point(190, 50);
            this.txtSMode.Name = "txtSMode";
            this.txtSMode.Size = new System.Drawing.Size(72, 20);
            this.txtSMode.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(137, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "S Mode:";
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(59, 50);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(72, 20);
            this.txtVersion.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Version:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(892, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chooseFileToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // chooseFileToolStripMenuItem
            // 
            this.chooseFileToolStripMenuItem.Name = "chooseFileToolStripMenuItem";
            this.chooseFileToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.chooseFileToolStripMenuItem.Text = "Open New File";
            this.chooseFileToolStripMenuItem.Click += new System.EventHandler(this.chooseFileToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // rBtnEuro
            // 
            this.rBtnEuro.AutoSize = true;
            this.rBtnEuro.Location = new System.Drawing.Point(835, 28);
            this.rBtnEuro.Name = "rBtnEuro";
            this.rBtnEuro.Size = new System.Drawing.Size(47, 17);
            this.rBtnEuro.TabIndex = 21;
            this.rBtnEuro.TabStop = true;
            this.rBtnEuro.Text = "Euro";
            this.rBtnEuro.UseVisualStyleBackColor = true;
            this.rBtnEuro.CheckedChanged += new System.EventHandler(this.rBtnEuro_CheckedChanged);
            // 
            // rBtnUS
            // 
            this.rBtnUS.AutoSize = true;
            this.rBtnUS.Location = new System.Drawing.Point(835, 51);
            this.rBtnUS.Name = "rBtnUS";
            this.rBtnUS.Size = new System.Drawing.Size(43, 17);
            this.rBtnUS.TabIndex = 22;
            this.rBtnUS.TabStop = true;
            this.rBtnUS.Text = "U.S";
            this.rBtnUS.UseVisualStyleBackColor = true;
            // 
            // lblUnits
            // 
            this.lblUnits.AutoSize = true;
            this.lblUnits.Location = new System.Drawing.Point(795, 53);
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Size = new System.Drawing.Size(34, 13);
            this.lblUnits.TabIndex = 23;
            this.lblUnits.Text = "Units:";
            // 
            // txtMaxSpeed
            // 
            this.txtMaxSpeed.Location = new System.Drawing.Point(82, 482);
            this.txtMaxSpeed.Name = "txtMaxSpeed";
            this.txtMaxSpeed.Size = new System.Drawing.Size(72, 20);
            this.txtMaxSpeed.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 485);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Max Speed:";
            // 
            // txtDistance
            // 
            this.txtDistance.Location = new System.Drawing.Point(247, 482);
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.Size = new System.Drawing.Size(72, 20);
            this.txtDistance.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(189, 485);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Distance:";
            // 
            // txtMaxAlt
            // 
            this.txtMaxAlt.Location = new System.Drawing.Point(715, 482);
            this.txtMaxAlt.Name = "txtMaxAlt";
            this.txtMaxAlt.Size = new System.Drawing.Size(72, 20);
            this.txtMaxAlt.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(641, 485);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Max Altitude:";
            // 
            // txtMaxPower
            // 
            this.txtMaxPower.Location = new System.Drawing.Point(563, 482);
            this.txtMaxPower.Name = "txtMaxPower";
            this.txtMaxPower.Size = new System.Drawing.Size(72, 20);
            this.txtMaxPower.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(494, 485);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Max Power:";
            // 
            // txtMinHR
            // 
            this.txtMinHR.Location = new System.Drawing.Point(416, 482);
            this.txtMinHR.Name = "txtMinHR";
            this.txtMinHR.Size = new System.Drawing.Size(72, 20);
            this.txtMinHR.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(328, 485);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Min Heart Rate:";
            // 
            // txtAvSpeed
            // 
            this.txtAvSpeed.Location = new System.Drawing.Point(82, 508);
            this.txtAvSpeed.Name = "txtAvSpeed";
            this.txtAvSpeed.Size = new System.Drawing.Size(72, 20);
            this.txtAvSpeed.TabIndex = 43;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 511);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "Av. Speed:";
            // 
            // txtAvHR
            // 
            this.txtAvHR.Location = new System.Drawing.Point(247, 508);
            this.txtAvHR.Name = "txtAvHR";
            this.txtAvHR.Size = new System.Drawing.Size(72, 20);
            this.txtAvHR.TabIndex = 41;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(160, 511);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Av. Heart Rate:";
            // 
            // txtAvAlt
            // 
            this.txtAvAlt.Location = new System.Drawing.Point(715, 508);
            this.txtAvAlt.Name = "txtAvAlt";
            this.txtAvAlt.Size = new System.Drawing.Size(72, 20);
            this.txtAvAlt.TabIndex = 39;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(645, 511);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 38;
            this.label14.Text = "Av. Altitude:";
            // 
            // txtAvPower
            // 
            this.txtAvPower.Location = new System.Drawing.Point(563, 508);
            this.txtAvPower.Name = "txtAvPower";
            this.txtAvPower.Size = new System.Drawing.Size(72, 20);
            this.txtAvPower.TabIndex = 37;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(498, 511);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "Av. Power:";
            // 
            // txtMaxHR
            // 
            this.txtMaxHR.Location = new System.Drawing.Point(416, 508);
            this.txtMaxHR.Name = "txtMaxHR";
            this.txtMaxHR.Size = new System.Drawing.Size(72, 20);
            this.txtMaxHR.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(325, 511);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Max Heart Rate:";
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(43, 28);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 13);
            this.lblFileName.TabIndex = 44;
            // 
            // txtNoRows
            // 
            this.txtNoRows.Location = new System.Drawing.Point(812, 508);
            this.txtNoRows.Name = "txtNoRows";
            this.txtNoRows.Size = new System.Drawing.Size(66, 20);
            this.txtNoRows.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(809, 492);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "No. of Rows:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 543);
            this.Controls.Add(this.txtNoRows);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.txtAvSpeed);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtAvHR);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtAvAlt);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtAvPower);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtMaxHR);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtMaxSpeed);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDistance);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtMaxAlt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMaxPower);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtMinHR);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblUnits);
            this.Controls.Add(this.rBtnUS);
            this.Controls.Add(this.rBtnEuro);
            this.Controls.Add(this.txtVersion);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtSMode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtLength);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtStartTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lboText);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Information";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lboText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtStartTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSMode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chooseFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Label lblUnits;
        public System.Windows.Forms.RadioButton rBtnEuro;
        public System.Windows.Forms.RadioButton rBtnUS;
        public System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox txtMaxSpeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDistance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMaxAlt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaxPower;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMinHR;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAvSpeed;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAvHR;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAvAlt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtAvPower;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMaxHR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtNoRows;
        public System.Windows.Forms.TextBox txtLength;
        public System.Windows.Forms.TextBox txtInterval;
    }
}

