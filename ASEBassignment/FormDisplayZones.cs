﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ASEBassignment
{
    public partial class FormDisplayZones : Form
    {
        int zone1, zone2, zone3, zone4, zone5, zone6, minX, maxX, totalRows;
        double zone1P, zone2P, zone3P, zone4P, zone5P, zone6P;
        string z1Key, z2Key, z3Key, z4Key, z5Key, z6Key, chartTitle, fileName;
        Form1 formData;

        public FormDisplayZones(string zoneType, double limit, int min, int max, string fName)
        {
            InitializeComponent();
            zone1 = zone2 = zone3 = zone4 = zone5 = zone6 = 0;
            minX = min;
            maxX = max;
            totalRows = maxX - minX;            //max and min X values from main graph
            fileName = fName;
            getPercentageValues(zoneType, limit);
        }

        private void FormDisplayZones_Load(object sender, EventArgs e)
        {
            createGraph(zedGraphControlZones);
        }

        private void getPercentageValues(string zoneType, double limit)
        {
            int i, columnIndex;
            double z1, z2, z3, z4, z5, z6, value;

            formData = new Form1(fileName);

            if (totalRows > Convert.ToInt16(formData.txtNoRows.Text))
            {
                totalRows = Convert.ToInt16(formData.txtNoRows.Text);
            }

            if (zoneType.Equals("FTP"))          //Work out values for each Power Zone
            {
                chartTitle = "Power Zones";
                z1 = (limit / 100) * 55;    z1Key = "Active Recovery";      //Active Recovery
                z2 = (limit / 100) * 75;    z2Key = "Endurance";            //Endurance
                z3 = (limit / 100) * 90;    z3Key = "Tempo";                //Tempo
                z4 = (limit / 100) * 105;   z4Key = "Lactate Threshold";    //Lactate Threshold
                z5 = (limit / 100) * 120;   z5Key = "VO2 Max";              //VO2 Max
                z6 = (limit / 100) * 150;   z6Key = "Anaerobic Capacity";   //Anaerobic Capacity

                columnIndex = 6;
                for (i = minX; i < totalRows; i++)
                {
                    value = Convert.ToDouble(formData.dgvData.Rows[i].Cells[columnIndex].Value);

                    if (value <= z1)
                        zone1++;
                    else if (value > z1 && value <= z2)
                        zone2++;
                    else if (value > z2 && value <= z3)
                        zone3++;
                    else if (value > z3 && value <= z4)
                        zone4++;
                    else if (value > z4 && value <= z5)
                        zone5++;
                    else if (value > z5 && value <= z6)
                        zone6++;
                }
            }

            if (zoneType.Equals("HR"))           //Work out values for each Heart Rate Zone
            {
                chartTitle = "Heart Rate Zones";
                z1 = (limit / 100) * 65;    z1Key = "Easy Ride";            //Easy Ride
                z2 = (limit / 100) * 75;    z2Key = "Basic Training";       //Basic Training
                z3 = (limit / 100) * 82;    z3Key = "Endurance";            //Endurance
                z4 = (limit / 100) * 89;    z4Key = "Race";                 //Race
                z5 = (limit / 100) * 94;    z5Key = "Anaerobic Threshold";  //Anaerobic Threshold
                z6 = (limit / 100) * 100;   z6Key = "High Intensity";       //High Intensity
                double z7 = (limit / 100) * 60;                                //Lower Limit

                columnIndex = 2;
                for (i = minX; i < totalRows; i++)
                {
                    value = Convert.ToDouble(formData.dgvData.Rows[i].Cells[columnIndex].Value);

                    if (value > z7 && value <= z1)
                        zone1++;
                    else if (value > z1 && value <= z2)
                        zone2++;
                    else if (value > z2 && value <= z3)
                        zone3++;
                    else if (value > z3 && value <= z4)
                        zone4++;
                    else if (value > z4 && value <= z5)
                        zone5++;
                    else if (value > z5 && value <= z6)
                        zone6++;
                }
            }
            this.Text = chartTitle;

            //Find zone percentages
            zone1P = (zone1 * 100) / totalRows;
            zone2P = (zone2 * 100) / totalRows;
            zone3P = (zone3 * 100) / totalRows;
            zone4P = (zone4 * 100) / totalRows;
            zone5P = (zone5 * 100) / totalRows;
            zone6P = (zone6 * 100) / totalRows;
        }

        // Build the Pie Chart
        private void createGraph(ZedGraphControl zgc)
        {
            GraphPane myPane = zgc.GraphPane;

            //Set graphPane Title
            myPane.Title.Text = chartTitle;

            //Background Colour
            myPane.Fill = new Fill(Color.White, Color.Goldenrod, 45.0f);
            myPane.Chart.Fill.Type = FillType.None;

            //Add Pie Slices
            PieItem segment1 = myPane.AddPieSlice(zone1, Color.LimeGreen, Color.White, 45f, 0, z1Key + " - " + zone1P + "%");
            PieItem segment2 = myPane.AddPieSlice(zone2, Color.Yellow, Color.White, 45f, .0, z2Key + " - " + zone2P + "%");
            PieItem segment3 = myPane.AddPieSlice(zone3, Color.Orange, Color.White, 45f, 0, z3Key + " - " + zone3P + "%");
            PieItem segment4 = myPane.AddPieSlice(zone4, Color.OrangeRed, Color.White, 45f, 0.2, z4Key + " - " + zone4P + "%");
            PieItem segment5 = myPane.AddPieSlice(zone5, Color.Red, Color.White, 45f, 0, z5Key + " - " + zone5P + "%");
            PieItem segment6 = myPane.AddPieSlice(zone6, Color.DarkRed, Color.White, 45f, 0.2, z6Key + " - " + zone6P + "%");

            //Calculate Axis Scale
            zgc.AxisChange();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
