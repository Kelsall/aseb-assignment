﻿namespace ASEBassignment
{
    partial class FormDisplayGraphs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heartRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altitudeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.cBoxDistance = new System.Windows.Forms.CheckBox();
            this.cBoxSpeed = new System.Windows.Forms.CheckBox();
            this.cBoxHeart = new System.Windows.Forms.CheckBox();
            this.cBoxPower = new System.Windows.Forms.CheckBox();
            this.cBoxAltitude = new System.Windows.Forms.CheckBox();
            this.txtMaxX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMinX = new System.Windows.Forms.TextBox();
            this.txtDistance = new System.Windows.Forms.TextBox();
            this.txtMaxSpeed = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMinHR = new System.Windows.Forms.TextBox();
            this.txtMaxPower = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaxAlt = new System.Windows.Forms.TextBox();
            this.txtAvSpeed = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAvHR = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAvPower = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaxHR = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAvAlt = new System.Windows.Forms.TextBox();
            this.txtIF = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTSS = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNP = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFTP = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtUserHR = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnHRZones = new System.Windows.Forms.Button();
            this.btnPowerZones = new System.Windows.Forms.Button();
            this.btnMetrics = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFilename = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtminHR1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMS1 = new System.Windows.Forms.TextBox();
            this.txtAS1 = new System.Windows.Forms.TextBox();
            this.txtAHR1 = new System.Windows.Forms.TextBox();
            this.txtD1 = new System.Windows.Forms.TextBox();
            this.txtmaxHR1 = new System.Windows.Forms.TextBox();
            this.txtmaxX1 = new System.Windows.Forms.TextBox();
            this.txtminX1 = new System.Windows.Forms.TextBox();
            this.txtMA1 = new System.Windows.Forms.TextBox();
            this.txtMP1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtAA1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtAP1 = new System.Windows.Forms.TextBox();
            this.txtmaxX2 = new System.Windows.Forms.TextBox();
            this.txtminX2 = new System.Windows.Forms.TextBox();
            this.txtMA2 = new System.Windows.Forms.TextBox();
            this.txtMP2 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtAA2 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtAP2 = new System.Windows.Forms.TextBox();
            this.txtmaxHR2 = new System.Windows.Forms.TextBox();
            this.txtD2 = new System.Windows.Forms.TextBox();
            this.txtAHR2 = new System.Windows.Forms.TextBox();
            this.txtAS2 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtminHR2 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtMS2 = new System.Windows.Forms.TextBox();
            this.btnClear1 = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnClear2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.graphToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1064, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDataToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // showDataToolStripMenuItem
            // 
            this.showDataToolStripMenuItem.Name = "showDataToolStripMenuItem";
            this.showDataToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.showDataToolStripMenuItem.Text = "Show Data";
            this.showDataToolStripMenuItem.Click += new System.EventHandler(this.showDataToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // graphToolStripMenuItem
            // 
            this.graphToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.distanceToolStripMenuItem,
            this.speedToolStripMenuItem,
            this.heartRateToolStripMenuItem,
            this.powerToolStripMenuItem,
            this.altitudeToolStripMenuItem,
            this.showAllToolStripMenuItem});
            this.graphToolStripMenuItem.Name = "graphToolStripMenuItem";
            this.graphToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.graphToolStripMenuItem.Text = "Graph";
            // 
            // distanceToolStripMenuItem
            // 
            this.distanceToolStripMenuItem.Name = "distanceToolStripMenuItem";
            this.distanceToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.distanceToolStripMenuItem.Text = "Distance";
            this.distanceToolStripMenuItem.Click += new System.EventHandler(this.distanceToolStripMenuItem_Click);
            // 
            // speedToolStripMenuItem
            // 
            this.speedToolStripMenuItem.Name = "speedToolStripMenuItem";
            this.speedToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.speedToolStripMenuItem.Text = "Speed";
            this.speedToolStripMenuItem.Click += new System.EventHandler(this.speedToolStripMenuItem_Click);
            // 
            // heartRateToolStripMenuItem
            // 
            this.heartRateToolStripMenuItem.Name = "heartRateToolStripMenuItem";
            this.heartRateToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.heartRateToolStripMenuItem.Text = "Heart Rate";
            this.heartRateToolStripMenuItem.Click += new System.EventHandler(this.heartRateToolStripMenuItem_Click);
            // 
            // powerToolStripMenuItem
            // 
            this.powerToolStripMenuItem.Name = "powerToolStripMenuItem";
            this.powerToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.powerToolStripMenuItem.Text = "Power";
            this.powerToolStripMenuItem.Click += new System.EventHandler(this.powerToolStripMenuItem_Click);
            // 
            // altitudeToolStripMenuItem
            // 
            this.altitudeToolStripMenuItem.Name = "altitudeToolStripMenuItem";
            this.altitudeToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.altitudeToolStripMenuItem.Text = "Altitude";
            this.altitudeToolStripMenuItem.Click += new System.EventHandler(this.altitudeToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem
            // 
            this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
            this.showAllToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.showAllToolStripMenuItem.Text = "Show All";
            this.showAllToolStripMenuItem.Click += new System.EventHandler(this.showAllToolStripMenuItem_Click);
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(13, 51);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(1040, 500);
            this.zedGraphControl1.TabIndex = 1;
            // 
            // cBoxDistance
            // 
            this.cBoxDistance.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBoxDistance.Checked = true;
            this.cBoxDistance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxDistance.Location = new System.Drawing.Point(13, 561);
            this.cBoxDistance.Name = "cBoxDistance";
            this.cBoxDistance.Size = new System.Drawing.Size(75, 50);
            this.cBoxDistance.TabIndex = 2;
            this.cBoxDistance.Text = "Distance";
            this.cBoxDistance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxDistance.UseVisualStyleBackColor = true;
            this.cBoxDistance.CheckedChanged += new System.EventHandler(this.cBoxDistance_CheckedChanged);
            // 
            // cBoxSpeed
            // 
            this.cBoxSpeed.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBoxSpeed.Checked = true;
            this.cBoxSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxSpeed.Location = new System.Drawing.Point(94, 561);
            this.cBoxSpeed.Name = "cBoxSpeed";
            this.cBoxSpeed.Size = new System.Drawing.Size(75, 50);
            this.cBoxSpeed.TabIndex = 3;
            this.cBoxSpeed.Text = "Speed";
            this.cBoxSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxSpeed.UseVisualStyleBackColor = true;
            this.cBoxSpeed.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cBoxHeart
            // 
            this.cBoxHeart.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBoxHeart.Checked = true;
            this.cBoxHeart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxHeart.Location = new System.Drawing.Point(175, 561);
            this.cBoxHeart.Name = "cBoxHeart";
            this.cBoxHeart.Size = new System.Drawing.Size(75, 50);
            this.cBoxHeart.TabIndex = 4;
            this.cBoxHeart.Text = "Heart Rate";
            this.cBoxHeart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxHeart.UseVisualStyleBackColor = true;
            this.cBoxHeart.CheckedChanged += new System.EventHandler(this.cBoxHeart_CheckedChanged);
            // 
            // cBoxPower
            // 
            this.cBoxPower.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBoxPower.Checked = true;
            this.cBoxPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxPower.Location = new System.Drawing.Point(13, 617);
            this.cBoxPower.Name = "cBoxPower";
            this.cBoxPower.Size = new System.Drawing.Size(75, 50);
            this.cBoxPower.TabIndex = 5;
            this.cBoxPower.Text = "Power";
            this.cBoxPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxPower.UseVisualStyleBackColor = true;
            this.cBoxPower.CheckedChanged += new System.EventHandler(this.cBoxPower_CheckedChanged);
            // 
            // cBoxAltitude
            // 
            this.cBoxAltitude.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBoxAltitude.Checked = true;
            this.cBoxAltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxAltitude.Location = new System.Drawing.Point(94, 617);
            this.cBoxAltitude.Name = "cBoxAltitude";
            this.cBoxAltitude.Size = new System.Drawing.Size(75, 50);
            this.cBoxAltitude.TabIndex = 6;
            this.cBoxAltitude.Text = "Altitude";
            this.cBoxAltitude.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cBoxAltitude.UseVisualStyleBackColor = true;
            this.cBoxAltitude.CheckedChanged += new System.EventHandler(this.cBoxAltitude_CheckedChanged);
            // 
            // txtMaxX
            // 
            this.txtMaxX.Location = new System.Drawing.Point(696, 665);
            this.txtMaxX.Name = "txtMaxX";
            this.txtMaxX.Size = new System.Drawing.Size(80, 20);
            this.txtMaxX.TabIndex = 7;
            this.txtMaxX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaxX_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(473, 668);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Min X:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(642, 668);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Max X:";
            // 
            // txtMinX
            // 
            this.txtMinX.Location = new System.Drawing.Point(516, 665);
            this.txtMinX.Name = "txtMinX";
            this.txtMinX.Size = new System.Drawing.Size(82, 20);
            this.txtMinX.TabIndex = 11;
            this.txtMinX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMinX_KeyDown);
            // 
            // txtDistance
            // 
            this.txtDistance.Location = new System.Drawing.Point(696, 561);
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.Size = new System.Drawing.Size(80, 20);
            this.txtDistance.TabIndex = 12;
            // 
            // txtMaxSpeed
            // 
            this.txtMaxSpeed.Location = new System.Drawing.Point(343, 561);
            this.txtMaxSpeed.Name = "txtMaxSpeed";
            this.txtMaxSpeed.Size = new System.Drawing.Size(80, 20);
            this.txtMaxSpeed.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 564);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Max Speed:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(638, 564);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Distance:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(624, 616);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Max. Power:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(252, 590);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Min. Heart Rate:";
            // 
            // txtMinHR
            // 
            this.txtMinHR.Location = new System.Drawing.Point(343, 587);
            this.txtMinHR.Name = "txtMinHR";
            this.txtMinHR.Size = new System.Drawing.Size(80, 20);
            this.txtMinHR.TabIndex = 18;
            // 
            // txtMaxPower
            // 
            this.txtMaxPower.Location = new System.Drawing.Point(696, 613);
            this.txtMaxPower.Name = "txtMaxPower";
            this.txtMaxPower.Size = new System.Drawing.Size(80, 20);
            this.txtMaxPower.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(452, 564);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Av. Speed:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(619, 642);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Max. Altitude:";
            // 
            // txtMaxAlt
            // 
            this.txtMaxAlt.Location = new System.Drawing.Point(696, 639);
            this.txtMaxAlt.Name = "txtMaxAlt";
            this.txtMaxAlt.Size = new System.Drawing.Size(80, 20);
            this.txtMaxAlt.TabIndex = 22;
            // 
            // txtAvSpeed
            // 
            this.txtAvSpeed.Location = new System.Drawing.Point(516, 561);
            this.txtAvSpeed.Name = "txtAvSpeed";
            this.txtAvSpeed.Size = new System.Drawing.Size(80, 20);
            this.txtAvSpeed.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(429, 590);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Av. Heart Rate:";
            // 
            // txtAvHR
            // 
            this.txtAvHR.Location = new System.Drawing.Point(516, 587);
            this.txtAvHR.Name = "txtAvHR";
            this.txtAvHR.Size = new System.Drawing.Size(80, 20);
            this.txtAvHR.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(453, 616);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Av. Power:";
            // 
            // txtAvPower
            // 
            this.txtAvPower.Location = new System.Drawing.Point(516, 613);
            this.txtAvPower.Name = "txtAvPower";
            this.txtAvPower.Size = new System.Drawing.Size(80, 20);
            this.txtAvPower.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(602, 590);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Max. Heart Rate:";
            // 
            // txtMaxHR
            // 
            this.txtMaxHR.Location = new System.Drawing.Point(696, 587);
            this.txtMaxHR.Name = "txtMaxHR";
            this.txtMaxHR.Size = new System.Drawing.Size(80, 20);
            this.txtMaxHR.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(448, 642);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Av. Altitude:";
            // 
            // txtAvAlt
            // 
            this.txtAvAlt.Location = new System.Drawing.Point(516, 639);
            this.txtAvAlt.Name = "txtAvAlt";
            this.txtAvAlt.Size = new System.Drawing.Size(80, 20);
            this.txtAvAlt.TabIndex = 31;
            // 
            // txtIF
            // 
            this.txtIF.Location = new System.Drawing.Point(343, 665);
            this.txtIF.Name = "txtIF";
            this.txtIF.Size = new System.Drawing.Size(80, 20);
            this.txtIF.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(315, 668);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "IF:";
            // 
            // txtTSS
            // 
            this.txtTSS.Location = new System.Drawing.Point(343, 639);
            this.txtTSS.Name = "txtTSS";
            this.txtTSS.Size = new System.Drawing.Size(80, 20);
            this.txtTSS.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(302, 642);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "TSS:";
            // 
            // txtNP
            // 
            this.txtNP.Location = new System.Drawing.Point(343, 613);
            this.txtNP.Name = "txtNP";
            this.txtNP.Size = new System.Drawing.Size(80, 20);
            this.txtNP.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(309, 617);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "NP:";
            // 
            // txtFTP
            // 
            this.txtFTP.Location = new System.Drawing.Point(897, 644);
            this.txtFTP.Name = "txtFTP";
            this.txtFTP.Size = new System.Drawing.Size(50, 20);
            this.txtFTP.TabIndex = 40;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(827, 647);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "User FTP:";
            // 
            // txtUserHR
            // 
            this.txtUserHR.Location = new System.Drawing.Point(897, 617);
            this.txtUserHR.Name = "txtUserHR";
            this.txtUserHR.Size = new System.Drawing.Size(50, 20);
            this.txtUserHR.TabIndex = 42;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(805, 620);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 41;
            this.label18.Text = "User Max HR:";
            // 
            // btnHRZones
            // 
            this.btnHRZones.Location = new System.Drawing.Point(847, 561);
            this.btnHRZones.Name = "btnHRZones";
            this.btnHRZones.Size = new System.Drawing.Size(100, 50);
            this.btnHRZones.TabIndex = 43;
            this.btnHRZones.Text = "HR Zones";
            this.btnHRZones.UseVisualStyleBackColor = true;
            this.btnHRZones.Click += new System.EventHandler(this.btnHRZones_Click);
            // 
            // btnPowerZones
            // 
            this.btnPowerZones.Location = new System.Drawing.Point(953, 561);
            this.btnPowerZones.Name = "btnPowerZones";
            this.btnPowerZones.Size = new System.Drawing.Size(100, 50);
            this.btnPowerZones.TabIndex = 44;
            this.btnPowerZones.Text = "Power Zones";
            this.btnPowerZones.UseVisualStyleBackColor = true;
            this.btnPowerZones.Click += new System.EventHandler(this.btnPowerZones_Click);
            // 
            // btnMetrics
            // 
            this.btnMetrics.Location = new System.Drawing.Point(953, 617);
            this.btnMetrics.Name = "btnMetrics";
            this.btnMetrics.Size = new System.Drawing.Size(100, 50);
            this.btnMetrics.TabIndex = 45;
            this.btnMetrics.Text = "NP / TSS / IF";
            this.btnMetrics.UseVisualStyleBackColor = true;
            this.btnMetrics.Click += new System.EventHandler(this.btnMetrics_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.Location = new System.Drawing.Point(175, 617);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 50);
            this.checkBox1.TabIndex = 46;
            this.checkBox1.Text = "Normalised Power";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 47;
            this.label3.Text = "File:";
            // 
            // lblFilename
            // 
            this.lblFilename.AutoSize = true;
            this.lblFilename.Location = new System.Drawing.Point(51, 28);
            this.lblFilename.Name = "lblFilename";
            this.lblFilename.Size = new System.Drawing.Size(0, 13);
            this.lblFilename.TabIndex = 48;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnCompare);
            this.panel1.Controls.Add(this.btnClear1);
            this.panel1.Controls.Add(this.txtmaxX1);
            this.panel1.Controls.Add(this.txtminX1);
            this.panel1.Controls.Add(this.txtMA1);
            this.panel1.Controls.Add(this.txtMP1);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.txtAA1);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.txtAP1);
            this.panel1.Controls.Add(this.txtmaxHR1);
            this.panel1.Controls.Add(this.txtD1);
            this.panel1.Controls.Add(this.txtAHR1);
            this.panel1.Controls.Add(this.txtAS1);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.txtminHR1);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txtMS1);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Location = new System.Drawing.Point(16, 702);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 173);
            this.panel1.TabIndex = 49;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnClearAll);
            this.panel2.Controls.Add(this.btnClear2);
            this.panel2.Controls.Add(this.txtmaxX2);
            this.panel2.Controls.Add(this.txtminX2);
            this.panel2.Controls.Add(this.txtMA2);
            this.panel2.Controls.Add(this.txtMP2);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.txtAA2);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.txtAP2);
            this.panel2.Controls.Add(this.txtmaxHR2);
            this.panel2.Controls.Add(this.txtD2);
            this.panel2.Controls.Add(this.txtAHR2);
            this.panel2.Controls.Add(this.txtAS2);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.txtminHR2);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.txtMS2);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Location = new System.Drawing.Point(552, 702);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(500, 173);
            this.panel2.TabIndex = 50;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(139, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Selected Information 1:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(139, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Selected Information 2:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(322, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 40;
            this.label21.Text = "Max. Heart Rate:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(170, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(81, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "Av. Heart Rate:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(193, 32);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 36;
            this.label23.Text = "Av. Speed:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 13);
            this.label24.TabIndex = 34;
            this.label24.Text = "Min. Heart Rate:";
            // 
            // txtminHR1
            // 
            this.txtminHR1.Location = new System.Drawing.Point(107, 55);
            this.txtminHR1.Name = "txtminHR1";
            this.txtminHR1.Size = new System.Drawing.Size(57, 20);
            this.txtminHR1.TabIndex = 33;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(356, 32);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "Distance:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(37, 32);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 13);
            this.label26.TabIndex = 31;
            this.label26.Text = "Max Speed:";
            // 
            // txtMS1
            // 
            this.txtMS1.Location = new System.Drawing.Point(107, 29);
            this.txtMS1.Name = "txtMS1";
            this.txtMS1.Size = new System.Drawing.Size(57, 20);
            this.txtMS1.TabIndex = 30;
            // 
            // txtAS1
            // 
            this.txtAS1.Location = new System.Drawing.Point(259, 29);
            this.txtAS1.Name = "txtAS1";
            this.txtAS1.Size = new System.Drawing.Size(57, 20);
            this.txtAS1.TabIndex = 41;
            // 
            // txtAHR1
            // 
            this.txtAHR1.Location = new System.Drawing.Point(259, 55);
            this.txtAHR1.Name = "txtAHR1";
            this.txtAHR1.Size = new System.Drawing.Size(57, 20);
            this.txtAHR1.TabIndex = 42;
            // 
            // txtD1
            // 
            this.txtD1.Location = new System.Drawing.Point(414, 29);
            this.txtD1.Name = "txtD1";
            this.txtD1.Size = new System.Drawing.Size(57, 20);
            this.txtD1.TabIndex = 43;
            // 
            // txtmaxHR1
            // 
            this.txtmaxHR1.Location = new System.Drawing.Point(414, 55);
            this.txtmaxHR1.Name = "txtmaxHR1";
            this.txtmaxHR1.Size = new System.Drawing.Size(57, 20);
            this.txtmaxHR1.TabIndex = 44;
            // 
            // txtmaxX1
            // 
            this.txtmaxX1.Location = new System.Drawing.Point(414, 107);
            this.txtmaxX1.Name = "txtmaxX1";
            this.txtmaxX1.Size = new System.Drawing.Size(57, 20);
            this.txtmaxX1.TabIndex = 56;
            // 
            // txtminX1
            // 
            this.txtminX1.Location = new System.Drawing.Point(414, 81);
            this.txtminX1.Name = "txtminX1";
            this.txtminX1.Size = new System.Drawing.Size(57, 20);
            this.txtminX1.TabIndex = 55;
            // 
            // txtMA1
            // 
            this.txtMA1.Location = new System.Drawing.Point(259, 107);
            this.txtMA1.Name = "txtMA1";
            this.txtMA1.Size = new System.Drawing.Size(57, 20);
            this.txtMA1.TabIndex = 54;
            // 
            // txtMP1
            // 
            this.txtMP1.Location = new System.Drawing.Point(259, 81);
            this.txtMP1.Name = "txtMP1";
            this.txtMP1.Size = new System.Drawing.Size(57, 20);
            this.txtMP1.TabIndex = 53;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(368, 110);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 13);
            this.label27.TabIndex = 52;
            this.label27.Text = "Max X:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(180, 110);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 13);
            this.label28.TabIndex = 51;
            this.label28.Text = "Max. Altitude:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(185, 84);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 13);
            this.label29.TabIndex = 50;
            this.label29.Text = "Max. Power:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(37, 110);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 13);
            this.label30.TabIndex = 49;
            this.label30.Text = "Av. Altitude:";
            // 
            // txtAA1
            // 
            this.txtAA1.Location = new System.Drawing.Point(107, 107);
            this.txtAA1.Name = "txtAA1";
            this.txtAA1.Size = new System.Drawing.Size(57, 20);
            this.txtAA1.TabIndex = 48;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(368, 84);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 13);
            this.label31.TabIndex = 47;
            this.label31.Text = "Min X:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(42, 84);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 13);
            this.label32.TabIndex = 46;
            this.label32.Text = "Av. Power:";
            // 
            // txtAP1
            // 
            this.txtAP1.Location = new System.Drawing.Point(107, 81);
            this.txtAP1.Name = "txtAP1";
            this.txtAP1.Size = new System.Drawing.Size(57, 20);
            this.txtAP1.TabIndex = 45;
            // 
            // txtmaxX2
            // 
            this.txtmaxX2.Location = new System.Drawing.Point(416, 110);
            this.txtmaxX2.Name = "txtmaxX2";
            this.txtmaxX2.Size = new System.Drawing.Size(57, 20);
            this.txtmaxX2.TabIndex = 80;
            // 
            // txtminX2
            // 
            this.txtminX2.Location = new System.Drawing.Point(416, 84);
            this.txtminX2.Name = "txtminX2";
            this.txtminX2.Size = new System.Drawing.Size(57, 20);
            this.txtminX2.TabIndex = 79;
            // 
            // txtMA2
            // 
            this.txtMA2.Location = new System.Drawing.Point(261, 110);
            this.txtMA2.Name = "txtMA2";
            this.txtMA2.Size = new System.Drawing.Size(57, 20);
            this.txtMA2.TabIndex = 78;
            // 
            // txtMP2
            // 
            this.txtMP2.Location = new System.Drawing.Point(261, 84);
            this.txtMP2.Name = "txtMP2";
            this.txtMP2.Size = new System.Drawing.Size(57, 20);
            this.txtMP2.TabIndex = 77;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(370, 113);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 76;
            this.label33.Text = "Max X:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(182, 113);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 13);
            this.label34.TabIndex = 75;
            this.label34.Text = "Max. Altitude:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(187, 87);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 13);
            this.label35.TabIndex = 74;
            this.label35.Text = "Max. Power:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(39, 113);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(64, 13);
            this.label36.TabIndex = 73;
            this.label36.Text = "Av. Altitude:";
            // 
            // txtAA2
            // 
            this.txtAA2.Location = new System.Drawing.Point(109, 110);
            this.txtAA2.Name = "txtAA2";
            this.txtAA2.Size = new System.Drawing.Size(57, 20);
            this.txtAA2.TabIndex = 72;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(370, 87);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(37, 13);
            this.label37.TabIndex = 71;
            this.label37.Text = "Min X:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(44, 87);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 13);
            this.label38.TabIndex = 70;
            this.label38.Text = "Av. Power:";
            // 
            // txtAP2
            // 
            this.txtAP2.Location = new System.Drawing.Point(109, 84);
            this.txtAP2.Name = "txtAP2";
            this.txtAP2.Size = new System.Drawing.Size(57, 20);
            this.txtAP2.TabIndex = 69;
            // 
            // txtmaxHR2
            // 
            this.txtmaxHR2.Location = new System.Drawing.Point(416, 58);
            this.txtmaxHR2.Name = "txtmaxHR2";
            this.txtmaxHR2.Size = new System.Drawing.Size(57, 20);
            this.txtmaxHR2.TabIndex = 68;
            // 
            // txtD2
            // 
            this.txtD2.Location = new System.Drawing.Point(416, 32);
            this.txtD2.Name = "txtD2";
            this.txtD2.Size = new System.Drawing.Size(57, 20);
            this.txtD2.TabIndex = 67;
            // 
            // txtAHR2
            // 
            this.txtAHR2.Location = new System.Drawing.Point(261, 58);
            this.txtAHR2.Name = "txtAHR2";
            this.txtAHR2.Size = new System.Drawing.Size(57, 20);
            this.txtAHR2.TabIndex = 66;
            // 
            // txtAS2
            // 
            this.txtAS2.Location = new System.Drawing.Point(261, 32);
            this.txtAS2.Name = "txtAS2";
            this.txtAS2.Size = new System.Drawing.Size(57, 20);
            this.txtAS2.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(324, 61);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(88, 13);
            this.label39.TabIndex = 64;
            this.label39.Text = "Max. Heart Rate:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(172, 61);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(81, 13);
            this.label40.TabIndex = 63;
            this.label40.Text = "Av. Heart Rate:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(195, 35);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(60, 13);
            this.label41.TabIndex = 62;
            this.label41.Text = "Av. Speed:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(18, 61);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(85, 13);
            this.label42.TabIndex = 61;
            this.label42.Text = "Min. Heart Rate:";
            // 
            // txtminHR2
            // 
            this.txtminHR2.Location = new System.Drawing.Point(109, 58);
            this.txtminHR2.Name = "txtminHR2";
            this.txtminHR2.Size = new System.Drawing.Size(57, 20);
            this.txtminHR2.TabIndex = 60;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(358, 35);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(52, 13);
            this.label43.TabIndex = 59;
            this.label43.Text = "Distance:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(39, 35);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(64, 13);
            this.label44.TabIndex = 58;
            this.label44.Text = "Max Speed:";
            // 
            // txtMS2
            // 
            this.txtMS2.Location = new System.Drawing.Point(109, 32);
            this.txtMS2.Name = "txtMS2";
            this.txtMS2.Size = new System.Drawing.Size(57, 20);
            this.txtMS2.TabIndex = 57;
            // 
            // btnClear1
            // 
            this.btnClear1.Location = new System.Drawing.Point(251, 134);
            this.btnClear1.Name = "btnClear1";
            this.btnClear1.Size = new System.Drawing.Size(240, 30);
            this.btnClear1.TabIndex = 57;
            this.btnClear1.Text = "CLEAR";
            this.btnClear1.UseVisualStyleBackColor = true;
            this.btnClear1.Click += new System.EventHandler(this.btnClear1_Click);
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(5, 134);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(240, 30);
            this.btnCompare.TabIndex = 58;
            this.btnCompare.Text = "COMPARE";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(5, 135);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(240, 30);
            this.btnClearAll.TabIndex = 60;
            this.btnClearAll.Text = "CLEAR ALL";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // btnClear2
            // 
            this.btnClear2.Location = new System.Drawing.Point(251, 135);
            this.btnClear2.Name = "btnClear2";
            this.btnClear2.Size = new System.Drawing.Size(240, 30);
            this.btnClear2.TabIndex = 59;
            this.btnClear2.Text = "CLEAR";
            this.btnClear2.UseVisualStyleBackColor = true;
            this.btnClear2.Click += new System.EventHandler(this.btnClear2_Click);
            // 
            // FormDisplayGraphs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 887);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblFilename);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnMetrics);
            this.Controls.Add(this.btnPowerZones);
            this.Controls.Add(this.btnHRZones);
            this.Controls.Add(this.txtUserHR);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtFTP);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtNP);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtTSS);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtIF);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtAvAlt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtAvPower);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtMaxHR);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtAvHR);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMaxAlt);
            this.Controls.Add(this.txtAvSpeed);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtMinHR);
            this.Controls.Add(this.txtMaxPower);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMaxSpeed);
            this.Controls.Add(this.txtDistance);
            this.Controls.Add(this.txtMinX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMaxX);
            this.Controls.Add(this.cBoxAltitude);
            this.Controls.Add(this.cBoxPower);
            this.Controls.Add(this.cBoxHeart);
            this.Controls.Add(this.cBoxSpeed);
            this.Controls.Add(this.cBoxDistance);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormDisplayGraphs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Polar Cycle System";
            this.Load += new System.EventHandler(this.FormDisplayGraphs_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphToolStripMenuItem;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.ToolStripMenuItem distanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heartRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altitudeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem;
        private System.Windows.Forms.CheckBox cBoxDistance;
        private System.Windows.Forms.CheckBox cBoxSpeed;
        private System.Windows.Forms.CheckBox cBoxHeart;
        private System.Windows.Forms.CheckBox cBoxPower;
        private System.Windows.Forms.CheckBox cBoxAltitude;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDistance;
        private System.Windows.Forms.TextBox txtMaxSpeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMinHR;
        private System.Windows.Forms.TextBox txtMaxPower;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaxAlt;
        private System.Windows.Forms.TextBox txtAvSpeed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAvHR;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAvPower;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMaxHR;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAvAlt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtUserHR;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnHRZones;
        private System.Windows.Forms.Button btnPowerZones;
        public System.Windows.Forms.TextBox txtMaxX;
        public System.Windows.Forms.TextBox txtMinX;
        public System.Windows.Forms.TextBox txtIF;
        public System.Windows.Forms.TextBox txtTSS;
        public System.Windows.Forms.TextBox txtNP;
        public System.Windows.Forms.TextBox txtFTP;
        private System.Windows.Forms.Button btnMetrics;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFilename;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtmaxX1;
        private System.Windows.Forms.TextBox txtminX1;
        private System.Windows.Forms.TextBox txtMA1;
        private System.Windows.Forms.TextBox txtMP1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtAA1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtAP1;
        private System.Windows.Forms.TextBox txtmaxHR1;
        private System.Windows.Forms.TextBox txtD1;
        private System.Windows.Forms.TextBox txtAHR1;
        private System.Windows.Forms.TextBox txtAS1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtminHR1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMS1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Button btnClear1;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Button btnClear2;
        private System.Windows.Forms.TextBox txtmaxX2;
        private System.Windows.Forms.TextBox txtminX2;
        private System.Windows.Forms.TextBox txtMA2;
        private System.Windows.Forms.TextBox txtMP2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtAA2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtAP2;
        private System.Windows.Forms.TextBox txtmaxHR2;
        private System.Windows.Forms.TextBox txtD2;
        private System.Windows.Forms.TextBox txtAHR2;
        private System.Windows.Forms.TextBox txtAS2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtminHR2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtMS2;
    }
}